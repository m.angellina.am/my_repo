package vectors;

/*
Создать класс для описания точки (вектора) в двумерном пространстве.
Реализовать сложение, вычитание векторов с помощью методов, умножение
и деление вектора на скаляр также реализовать с помощью соответствующих
методов. Реализовать набор примеров для демонстрации работы указанных методов.
*/

public class Main{
  public static void main(String[] args){

  Vectors v1 = new Vectors(15, 8, 3, 4);
  Vectors v2 = new Vectors(5, 6, 20, 11);
  Vectors v3 = new Vectors(5, 10, 15, 20);


  System.out.println(v1.toString());
  System.out.println(v2.toString());

  System.out.println(Vectors.addition(v1, v2));
  System.out.println(Vectors.subtraction(v1, v2));

  System.out.println(v3.multiplyScalar(2));
  System.out.println(v3.divisionScalar(5));
  }
}
