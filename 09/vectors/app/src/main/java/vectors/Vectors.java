package vectors;

/*
Создать класс для описания точки (вектора) в двумерном пространстве.
Реализовать сложение, вычитание векторов с помощью методов, умножение
и деление вектора на скаляр также реализовать с помощью соответствующих
методов. Реализовать набор примеров для демонстрации работы указанных методов.
*/

public class Vectors {

    private double x1; // начало
    private double y1; // начало
    private double x2; // конец
    private double y2; // конец
    private double deltaX;
    private double deltaY;

    Vectors(double x1, double y1, double x2, double y2){
      this.x1 = x1;
      this.y1 = y1;
      this.x2 = x2;
      this.y2 = y2;
    }

    public double getX1() {
      return this.x1;
    }

    public void setX1(double x1) {
      this.x1 = x1;
    }

    public double getX2() {
      return this.x2;
    }

    public void setX2(double x2) {
      this.x2 = x2;
    }

    public double getY1() {
      return this.y1;
    }

    public void setY1(double y1) {
      this.y1 = y1;
    }

    public double getY2() {
      return this.y2;
    }

    public void setY2(double y2) {
      this.y2 = y2;
    }

    public double getDeltaX(){
      return this.x2 - this.x1;
    }

    public double getDeltaY(){
      return this.y2 - this.y1;
    }

    public String toString(){
      return this.x1 + " " + this.y1 + " " + this.x2 + " " + this.y2;
    }

    public static Vectors addition(Vectors v1, Vectors v2){
      Vectors readyV = new Vectors(v1.getX1(), v1.getY1(),
      v1.getX2() + v2.getDeltaX(), v1.getY2() + v2.getDeltaY());
      return readyV;
    }

    public static Vectors subtraction(Vectors v1, Vectors v2){
      Vectors readyV = new Vectors(v1.getX1(), v1.getY1(),
      v1.getX2() - v2.getDeltaX(), v1.getY2() - v2.getDeltaY());
      return readyV;
    }

    public Vectors multiplyScalar(double scalar){
      Vectors readyV = new Vectors(this.getX1(), this.getY1(),
      this.getX2()*scalar, this.getY2()*scalar);
      return readyV;
    }

    public Vectors divisionScalar(double scalar){
      Vectors readyV = new Vectors(this.getX1(), this.getY1(),
      this.getX2()/scalar, this.getY2()/scalar);
      return readyV;
    }
}
