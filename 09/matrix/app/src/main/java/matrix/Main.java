package matrix;

/*
Создать программу, реализующую класс с операциями для работы с двумерными матрицами.
Класс должен позволять производить сложение, вычитание, умножение матриц. Предусмотреть
возможные ошибки и корректно их обрабатывать. Реализовать набор примеров для демонстрации
работы класса.
*/

public class Main{
  public static void main(String[] args) throws MatrixException, NullPointerException {
    Operations op = new Operations();

    int n = 3;
    int m = 3;

    Matrix matrix1 = new Matrix(n, m);
    matrix1.generate();
    System.out.println("Matrix 1: ");
    op.printMatrix(matrix1);

    Matrix matrix2 = new Matrix(n, m);
    matrix2.generate();
    System.out.println("Matrix 2: ");
    op.printMatrix(matrix2);

    //сложение матриц
    int[][] additionMatrix = op.addition(matrix1, matrix2);
    System.out.println("Addition: ");
    op.printMatrix(additionMatrix);

    //вычитание матриц
    int[][] subtractionMatrix = op.subtraction(matrix1, matrix2);
    System.out.println("Subtraction: ");
    op.printMatrix(subtractionMatrix);

    //умножение матриц
    int[][] multiplyMatrix = op.multiply(matrix1, matrix2);
    System.out.println("Multiply: ");
    op.printMatrix(multiplyMatrix);
  }
}
