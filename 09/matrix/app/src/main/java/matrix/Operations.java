package matrix;

/*
Создать программу, реализующую класс с операциями для работы с двумерными матрицами.
Класс должен позволять производить сложение, вычитание, умножение матриц. Предусмотреть
возможные ошибки и корректно их обрабатывать. Реализовать набор примеров для демонстрации
работы класса.
*/

public class Operations{
  public static int[][] addition(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();

    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("matrix1 or matrix2 is ​​empty");
    }
    if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
      throw new MatrixException("matrices are not the same size");
    }

    result = new int[matrix1.length][matrix1[0].length];

    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix1[0].length; j++) {
        result[i][j] = matrix1[i][j] + matrix2[i][j];
      }
    }
    return result;
  }

  public static int[][] subtraction(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();

    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("matrix1 or matrix2 is ​​empty");
    }
    if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
      throw new MatrixException("matrices are not the same size");
    }

    result = new int[matrix1.length][matrix1[0].length];

    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix1[0].length; j++) {
        result[i][j] = matrix1[i][j] - matrix2[i][j];
      }
    }
    return result;
  }

  public static int[][] multiply(Matrix m1, Matrix m2) throws NullPointerException, MatrixException {
    int[][] result;
    int[][] matrix1 = m1.getMatrix();
    int[][] matrix2 = m2.getMatrix();

    if (matrix1 == null || matrix2 == null) {
      throw new NullPointerException("matrix1 or matrix2 is ​​empty");
    }
    if (matrix1.length != matrix2.length || matrix1[0].length != matrix2[0].length) {
      throw new MatrixException("matrices are not the same size");
    }

    result = new int[matrix1.length][matrix2[0].length];

    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix2[0].length; j++) {
        for (int l = 0; l < matrix2.length; l++) {
          result[i][j] += matrix1[i][l] * matrix2[l][j];
        }
      }
    }
    return result;
  }

  public void printMatrix(int[][] matrix){
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[0].length; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }

  public void printMatrix(Matrix m1){
    int[][] matrix = m1.getMatrix();
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[0].length; j++) {
        System.out.print(matrix[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }
}
