package matrix;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OperationExceptTest{
  Matrix matrix1 = new Matrix(3, 3);
  Matrix matrix2 = new Matrix(2, 3);
  Matrix nullMatrix1 = new Matrix();
  Matrix nullMatrix2 = new Matrix();
  Operations op = new Operations();

  @Test void SetMatrix() throws MatrixException{
    int[][] new_m = new int[][] {{1, 2, 3}, {3, 4, 5}, {6, 7, 8}};
    matrix1.setMatrix(new_m);
    assertArrayEquals(new_m, matrix1.getMatrix());
  }

  @Test void additionNullException() throws NullPointerException {
    NullPointerException except = assertThrows(NullPointerException.class, () -> op.addition(nullMatrix1, nullMatrix2));
    assertEquals("matrix1 or matrix2 is ​​empty", except.getMessage());
  }

  @Test void subtractionNullException() throws NullPointerException {
    NullPointerException except = assertThrows(NullPointerException.class, () -> op.subtraction(nullMatrix1, nullMatrix2));
    assertEquals("matrix1 or matrix2 is ​​empty", except.getMessage());
  }

  @Test void multiplyNullException() throws NullPointerException {
    NullPointerException except = assertThrows(NullPointerException.class, () -> op.multiply(nullMatrix1, nullMatrix2));
    assertEquals("matrix1 or matrix2 is ​​empty", except.getMessage());
  }

  @Test void additionLengthException() throws MatrixException {
    MatrixException except = assertThrows(MatrixException.class, () -> op.addition(matrix1, matrix2));
    assertEquals("matrices are not the same size", except.getMessage());
  }

  @Test void subtractionLengthException() throws MatrixException {
    MatrixException except = assertThrows(MatrixException.class, () -> op.subtraction(matrix1, matrix2));
    assertEquals("matrices are not the same size", except.getMessage());
  }

  @Test void multiplyLengthException() throws MatrixException {
    MatrixException except = assertThrows(MatrixException.class, () -> op.multiply(matrix1, matrix2));
    assertEquals("matrices are not the same size", except.getMessage());
  }
}
