package matrix;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {
  int[][] matrix1 = new int[][] {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
  int[][] matrix2 = new int[][] {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}};
  Matrix m1 = new Matrix(3, 3);
  Matrix m2 = new Matrix(3, 3);
  Operations op = new Operations();

  @Test void AdditionMatrix() throws MatrixException {
    m1.setMatrix(matrix1);
    m2.setMatrix(matrix2);
    int[][] result = new int[][] {{2, 4, 6}, {5, 7, 9}, {8, 10, 12}};
    assertArrayEquals(result, op.addition(m1, m2));
  }

  @Test void SubtractionMatrix() throws MatrixException {
    m1.setMatrix(matrix1);
    m2.setMatrix(matrix2);
    int[][] result = new int[][]{{0, 0, 0}, {3, 3, 3}, {6, 6, 6}};
    assertArrayEquals(result, op.subtraction(m1, m2));
  }

  @Test void MultiplyMatrix() throws MatrixException {
    m1.setMatrix(matrix1);
    m2.setMatrix(matrix2);
    int[][] result = new int[][]{{6, 12, 18}, {15, 30, 45}, {24, 48, 72}};
    assertArrayEquals(result, op.multiply(m1, m2));
  }
}
