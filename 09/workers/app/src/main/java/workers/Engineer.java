package workers;

/*
Создать классы для описания работников: работник в общем, бухгалтер,
главный бухгалтер, инженер, рабочий. Все имеют имя, должность и зарплату.
Добавить характерные для каждого класса в отдельности методы.
Переопределить все доступные методы класса Object адекватным образом.
Создать массив, содержащий объекты всех классов описания работников,
и вывести его на консоль.
*/
//инженер
public class Engineer extends Employee {
  public Engineer(String name, String prof, int salary) {
    super(name, prof, salary);
  }

  public String toString(){
    return "Hi! My name is " +
    getName() + ", my prof is " +
    getProf() + ", my salary is " +
    getSalary() + " and I am Engineer!";
  }

  public String character_engineer(){ //характерный метод
    return "I am an Engineer and I can desiign cool things!";
  }

}
