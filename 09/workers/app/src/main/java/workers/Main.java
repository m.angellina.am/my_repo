package workers;

class Main{

  public static void main(String[] args) {
    Employee[] a = new Employee[5];

    a[0] = new Accountant("A", "Accountant", 15000);
    a[1] = new CheifAccountant("B", "CheifAccountant", 30000);
    a[2] = new Engineer("C", "Engineer", 40000);
    a[3] = new Worker("D", "Worker", 12500);
    a[4] = new Worker("F", "Worker", 12500);

    for (int i = 0; i < a.length; i++){
      System.out.println(a[i].toString());
    }
  }

}
