package workers;
/*
Создать классы для описания работников: работник в общем, бухгалтер,
главный бухгалтер, инженер, рабочий. Все имеют имя, должность и зарплату.
Добавить характерные для каждого класса в отдельности методы.
Переопределить все доступные методы класса Object адекватным образом.
Создать массив, содержащий объекты всех классов описания работников,
и вывести его на консоль.
*/
// общий класс
public class Employee implements Cloneable {
  protected String name;
  protected String prof;
  protected int salary;

  public Employee(String name, String prof, int salary) {
    this.name = name;
    this.prof = prof;
    this.salary = salary;
  }

  public Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  public boolean equals(Object e){
    if (this == e) return true;
    if (e == null || getClass() != e.getClass()) return false;

    Employee e1 = (Employee) e;
    if (this.prof != e1.prof) return false;
    if (this.salary != e1.salary) return false;
    return this.name.equals(e1.name);
  }

  public int hashCode() {
    return name.length() * 100 + prof.length() * 10 + salary;
  }

  public String toString() {
    return "My name " + this.name +
           ", I am " + this.prof +
           " and my salary is " + this.salary;
  }

  public String getName() {
    return name;
  }

  public String getProf() {
    return prof;
  }

  public int getSalary() {
    return salary;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  public void setProf(String prof) {
    this.prof = prof;
  }
}
