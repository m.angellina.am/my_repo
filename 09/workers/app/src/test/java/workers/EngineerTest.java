package workers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class EngineerTest{

  private Engineer eng1;
  private Engineer eng2;

  @BeforeEach
  public void initialize(){
    eng1 = new Engineer("Den", "Engineer", 40000);
    eng2 = new Engineer("Dane", "Engineer", 40000);
  }

  @Test void testSalary() {
    assertEquals(40000, eng1.getSalary());
  }

  @Test void testWork() {
    assertEquals("I am an Engineer and I can desiign cool things!", eng1.character_engineer());
  }

  @Test void testEquals() {
    assertEquals(false, eng1.equals(eng2));
  }

  @Test void testHachCode() {
    assertNotEquals(eng2.hashCode(), eng1.hashCode());
  }
}
