package workers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class WorkerTest{

  private Worker worker1;
  private Worker worker2;

  @BeforeEach
  public void initialize(){
    worker1 = new Worker("Jon", "Worker", 12500);
    worker2 = new Worker("Jonnie", "Worker", 12500);
  }

  @Test void testSalary() {
    assertEquals(12500, worker1.getSalary());
  }

  @Test void testWork() {
    assertEquals("I am Worker and I can work", worker1.character_worker());
  }

  @Test void testEquals() {
    assertEquals(false, worker1.equals(worker2));
  }

  @Test void testHachCode() {
    assertNotEquals(worker2.hashCode(), worker1.hashCode());
  }
}
