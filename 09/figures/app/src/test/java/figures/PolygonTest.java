package figures;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class PolygonTest {
  private Polygon p1;

  @BeforeEach
  public void initialize(){
    p1 = new Polygon(5);
  }

  @Test void outputPolygon(){
    assertEquals("I am Polygon", p1.output());
  }
}
