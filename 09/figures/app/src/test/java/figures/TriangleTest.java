package figures;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class TriangleTest {
  private Triangle t1;

  @BeforeEach
  public void initialize(){
    t1 = new Triangle(5);
  }

  @Test void PerimeterTest(){
    assertEquals(15, t1.getPerimeter());
  }

  @Test void PerimeterStrTest(){
    assertEquals("I am Triangle and my perimeter is 15", t1.output());
  }
}
