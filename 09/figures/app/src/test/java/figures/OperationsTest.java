package figures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class OperationsTest {
     @Test void OperationsTest() {
       Polygon p1 = new Polygon(6);
       Triangle p2 = new Triangle(3);
       Triangle p3 = new Triangle(4);

       Text text1 = new Text("Hi");

       Output[] a = new Output[] {p1, p2, p3, text1};
       Operations operations = new Operations(a);
       String[] polygon = new String[]{
         "I am Polygon",
         "I am Triangle and my perimeter is 9",
         "I am Triangle and my perimeter is 12",
         "Hi" };
       assertArrayEquals(polygon, operations.outputPolygon());
     }
 }
