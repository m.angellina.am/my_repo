package figures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TextTest {
     @Test void textTest() {
         Text text1 = new Text("Wow!");
         assertEquals("Wow!", text1.output());
     }
 }
