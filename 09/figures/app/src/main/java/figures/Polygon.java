package figures;
/*
Создать класс для описания многоугольников в общем и треугольника в частности.
Также создать класс для описания текста. Все классы должны иметь метод вывода
на экран. Создать отдельный класс для операций над объектами. В классе должен
быть метод вывода массива объектов на экран. Все объекты задавать в исходном
коде (не меньше 3-х различных треугольников и двух текстов).
 */
 public class Polygon implements Output{
   protected int countSides;
   protected int sizeSide;

   Polygon(int sizeSide){
     this.sizeSide = sizeSide;
   }

   public int getPerimeter(){
     int perimeter = this.countSides * this.sizeSide;
     return perimeter;
   }

   public String toString(){
     return "Perimeter is " + this.getPerimeter();
   }

   public String output(){
       return "I am Polygon";
   }

 }
