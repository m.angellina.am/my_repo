import java.util.Scanner;

public class Factorial {
  public static void main(String[] args) {
    Scanner console = new Scanner(System.in);
    double a = console.nextDouble();
    double count = 1;
    for (int i = 1; i <= a; i++) {
      count = count*i;
    }
    System.out.println(count);
  }
}
