import java.util.Scanner;

public class rFactorial{
	static int rFactorial(int n) {
		if (n == 1) {
		    return 1;
		}
		else {
		    return n * rFactorial(n-1);
		}
	}
	public static void main(String[] args){
		Scanner console = new Scanner(System.in);
		int n = console.nextInt();
		System.out.println(rFactorial(n));
	}
}
