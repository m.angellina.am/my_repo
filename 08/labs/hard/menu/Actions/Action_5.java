package menu.Actions;

public class Action_5 extends Action{
  String title = "Вы находитесь в моей игре. Куда пойдете?";

  public Action_5(String title) {
    super(title);
  }

  public void act(){
    System.out.println("Вам повезло, вы выбрались из игры)" + "\n");
  }
}
