package menu.Actions;

public class Action_2 extends Action{
  String title = "Вы находитесь в моей игре. Куда пойдете?";

  public Action_2(String title) {
    super(title);
  }

  public void act(){
    System.out.println("Вы наткнулись на желтых динозавриков! Они подскажут, куда идти дальше"  + "\n");
  }
}
