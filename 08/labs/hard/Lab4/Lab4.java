package labs.hard.Lab4;
import java.util.Scanner;
import java.util.Date;
import java.util.Random;
/*
Создать программу, моделирующую случайный процесс.
С клавиатуры задается некий "порог" в виде вещественного числа,
коэффициент влияния порога и максимальное время моделирования.
В случайные моменты времени генерируются случайные вещественные
числа в интервале [-коэффициент*порог; коэффициент*порог].
Если сгенерированное число больше заданного порога, то прекратить
моделирование. По завершении моделирования вывести следующую
информацию на консоль: <время от начала эксперимента>: <число>
*/

public class Lab4{

    public static void work(double porog, double coeff, long max_time){

      Thread thread = new Thread();
      double counter = Math.random()*(2*(porog*coeff)) - (porog*coeff); //интервал [-коэффициент*порог; коэффициент*порог]

      long start = System.currentTimeMillis();
      long end = start;
      boolean flag = true;

      while (flag){
        if (counter <= porog && ((end - start) <= max_time)) {
          try {
            thread.sleep(new Random().nextInt(10));
          }
          catch (Exception e){}
          end = System.currentTimeMillis();
        }
        else{
          flag = false;
        }
        System.out.println("" + counter + " " + (end - start));
      }
    }
}
