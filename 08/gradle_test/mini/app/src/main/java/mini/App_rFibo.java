package mini;

import java.util.Scanner;

public class App_rFibo {

  public static int[] rFibo(int n){
    if (n <= 1){
      return new int[] {0};
    }
    if (n == 2) {
      return new int[] {0, 1};
    }

    int[] array = new int[n];
    array[0] = 0;
    array[1] = 1;
    return getFiboRec(2, array);
  }

  private static int[] getFiboRec(int i, int[] array){
    if (i == array.length) {
			return array;
		}
		else {
			array[i] = array[i - 1] + array[i - 2];
      i++;
			return getFiboRec(i, array);
    }
	}
}
