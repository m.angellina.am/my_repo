package mini;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest_rFibo {

    @Test void rFibo(){
      int[] target =  new int[] {0, 1, 1, 2, 3};
      int[] result = App_rFibo.rFibo(5);
      assertArrayEquals(target, result);
    }
}
