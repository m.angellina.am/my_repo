package matrix;
import java.util.Scanner;

/*
Пользователь вводит с клавиатуры количество матриц и их размер.
Создать матрицы и заполнить их случайными числами. Перемножить.
Вывести на консоль время, затраченное на умножение в первом,
реализовать для каждого достаточно большого действия отдельный метод.
*/
public class Main{
  public static void main(String[] args){
    Scanner sc = new Scanner(System.in);
    System.out.println("Введите количество матриц: ");
    int count = sc.nextInt();

    System.out.println("Введите размер матрицы(строки и столбцы): ");
    int size = sc.nextInt();

    MatrixGeneration[] matrix = new MatrixGeneration[count];
    for (int i = 0; i < count; i++){
      matrix[i] = new MatrixGeneration(size);
    }

    long time = System.currentTimeMillis();

    MatrixMultiply multiply = new MatrixMultiply();
    int[][] matrix2 = matrix[0].getMatrix();
    for (int i = 1; i < count; i++){
      matrix2 = multiply.Multiply(matrix2, matrix[i].getMatrix());
    }
    System.out.println("Время:");
    System.out.println(System.currentTimeMillis() - time);
    System.out.println();

    for (int i = 0; i < size; i++){
      for (int j = 0; j < size; j++){
        System.out.print(matrix2[i][j] + " ");
      }
      System.out.println();
    }
  }
}
