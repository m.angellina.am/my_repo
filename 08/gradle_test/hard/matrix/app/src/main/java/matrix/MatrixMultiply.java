package matrix;
import java.util.Random;

/*
Пользователь вводит с клавиатуры количество матриц и их размер.
Создать матрицы и заполнить их случайными числами. Перемножить.
Вывести на консоль время, затраченное на умножение в первом,
реализовать для каждого достаточно большого действия отдельный метод.
*/
public class MatrixMultiply{

  public static int[][] Multiply(int[][] matrix1, int[][] matrix2) {
    int[][] matrix3 = new int[matrix1.length][matrix1.length];

    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix2[i].length; j++) {
        int tmp = 0;
        for (int l = 0; l < matrix2[j].length; l++) {
          tmp += (matrix1[i][l] * matrix2[l][j]);
        }
        matrix3[i][j] = tmp;
      }
    }
    return matrix3;
  }
}
