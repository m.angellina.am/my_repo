package matrix;

/*
Пользователь вводит с клавиатуры количество матриц и их размер.
Создать матрицы и заполнить их случайными числами. Перемножить.
Вывести на консоль время, затраченное на умножение в первом,
реализовать для каждого достаточно большого действия отдельный метод.
*/
public class MatrixGeneration {

  private int[][] matrix;

  public int[][] getMatrix(){
    return matrix;
  }

  MatrixGeneration(int size){
    int[][] matrix = new int[size][size];
    for (int i = 0; i < size; i++){
      for (int j = 0; j < size; j++){
        matrix[i][j] = (int)(Math.random() * 100);
      }
    }
    this.matrix = matrix;
  }
}
