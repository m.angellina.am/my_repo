package sort;

class App_sort{
  public static void main(String[] args) {
    double[] array = {1, 5, 2, 9, 4, 6, 0, 3};
    sort(array);
    System.out.println();
    double[] sortarr = createSortedArray(array);
  }

  public static void sort(double[] arr){
    double temp = 0;
		int i = 0;
		while (i < arr.length - 1){
			if (arr[i] > arr[i+1]){
        temp = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = temp;
        i = 0;
      }else{
				i += 1;
			}
		}
    for (int j = 0; j < arr.length; j++) {
      System.out.print(arr[j] + " ");
    }
	}

  public static double[] createSortedArray(double[] arr){
    double[] newarr = arr.clone();
    sort(newarr);
    return newarr;
  }
}
