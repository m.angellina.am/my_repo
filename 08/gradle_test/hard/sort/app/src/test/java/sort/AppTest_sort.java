package sort;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest_sort {
    @Test void testSort() {
        double[] array = {1, 5, 2, 9, 4, 6, 0, 3};
        App_sort.sort(array);
        assertArrayEquals(new double[] {0, 1, 2, 3, 4, 5, 6, 9}, array);
    }

    @Test void testCreateSortedArray(){
      double[] array = {1, 4, 2, 6, 3, 9, 5, 8};
      assertArrayEquals(new double[] {1, 2, 3, 4, 5, 6, 8, 9}, App_sort.createSortedArray(array));
    }
}
