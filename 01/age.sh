#!/usr/bin/bash

read INPUT
k=0
VARData="$(date)"
while [[ $k -lt "2**$INPUT" ]]
do
    echo >>  README.md "$(( 2**$k ))"
let $(( k=$k+1 ))
done
VARFin="$(date)"
echo >>  README.md "$VARData"
echo >>  README.md "$VARFin"
cat README.md
