/*
Программа должна позволять генерировать и сохранять
отображение со 100 числовыми ключами и значениями в
виде объекта или просматривать сохраненное.
Предполагается, что пользователь должен вводить путь до
файла с отображением.
Задание подразумевает использование механизма сериализации.
*/
package serialization;

import java.util.Random;
import java.io.Serializable;

/**
 * Class Student for creating a student with
 * random name and course
 * @author Angelina Motrikala
 * @version 1.0
 * @see Main
 */
public class Student implements Serializable{

  /** @value unique identifier class for version control */
  private static final long serialVersionUID = -845811210152464645L;
  /** @value the field that stores the student's name*/
  private String firstName;
  /** @value the field that stores the student's second name*/
  private String secondName;
  /** @value the field that stores the student's course*/
  private int course;

  /**
   * constructor Student
   */
  Student(){
    this.firstName = nameGeneration();
    this.secondName = nameGeneration();
    this.course = courseGenration();
  }

  /**
   * method getFirstName
   * @return student's first name
   */
  public String getFirstName(){
    return this.firstName;
  }

  /**
   * method getFirstName
   * @return student's second name
   */
  public String getSecondName(){
    return this.secondName;
  }

  /**
   * method getFirstName
   * @return student's course
   */
  public int getCourse(){
    return this.course;
  }

  /**
  * method to generating a string with random characters
  * @return generated string uppercase
  */
  public String nameGeneration(){
    Random a = new Random();
    String str = "";
    for (int i = 0; i < 10; i++) {
        str += (char) (a.nextInt(26) + 'a');
    }
    return str.substring(0,1).toUpperCase() + str.substring(1);
  }

  /**
  * method to generating random number
  * @return generated number
  */
  public int courseGenration(){
    int value = 0;
    for (int i = 0; i < 100; i++){
      value = (int)(Math.random() * (6) + 1);
    }
    return value;
  }

  /**
  * override toString method
  * @return string with student data
  */
  public String toString(){
    return getFirstName() + " " +getSecondName() + " " + getCourse();
  }
}
