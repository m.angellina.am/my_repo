/*
Программа должна позволять генерировать и сохранять
отображение со 100 числовыми ключами и значениями в
виде объекта или просматривать сохраненное.
Предполагается, что пользователь должен вводить путь до
файла с отображением.
Задание подразумевает использование механизма сериализации.
*/
package serialization;

import java.util.Scanner;
import java.io.IOException;

public class Main{

  public static void main(String[] args){

    System.out.println("Введите путь до файла: ");
    Scanner scanner = new Scanner(System.in);
    String way = null;

    while(scanner.hasNext()){
      try{
        way = scanner.nextLine();
        if (way.equals("exit")){
          break;
        }
        else if (way != null && way.length() != 0){
          Action.serialize(way);
          Action.deserialize(way);
        }
        else{
          System.out.println("Вы ничего не ввели");
        }
      }
      catch (NumberFormatException e) {
        System.out.println("Неправильный формат ввода!");
      }
    }
  }
}
