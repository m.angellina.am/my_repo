package task2;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.OutputTimeUnit;

import java.util.concurrent.TimeUnit;
import java.util.Map;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class TestBenchmark {

  Maps hashmap = new Maps(new HashMap<String, Integer>());
  Maps treemap = new Maps(new TreeMap<String, Integer>());
  Maps hashtable = new Maps(new Hashtable<String, Integer>());
  Maps linkedhashmap = new Maps(new LinkedHashMap<String, Integer>());

  @Setup(Level.Trial) @Benchmark
  public void createHashMap(){
    Maps hashMap = new Maps(new HashMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void createTreeMap(){
    Maps treeMap = new Maps(new TreeMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void createLinkedHashMap(){
    Maps linkedHashMap = new Maps(new LinkedHashMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void createHashtable(){
    Maps hashtable = new Maps(new Hashtable<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void sumHashMap() {
    hashmap.sumValues(hashmap.arrayGeneration());
  }

  @Setup(Level.Trial) @Benchmark
  public void sumTreeMap() {
    treemap.sumValues(treemap.arrayGeneration());
  }

  @Setup(Level.Trial) @Benchmark
  public void sumLinkedHashMap() {
    linkedhashmap.sumValues(linkedhashmap.arrayGeneration());
  }

  @Setup(Level.Trial) @Benchmark
  public void sumHashtable() {
    hashtable.sumValues(hashtable.arrayGeneration());
  }
}
