/*
Создать отображение размером в 2 000 000 записей. ok
Ключ - строка из 10 случайных символов.  ok
Значение - случайное число. ok
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.
 */
package task1;

import java.util.*;

public class Main{

  public static void main(String[] args){

    double timestart = System.currentTimeMillis();

    // Maps map1 = new Maps(new HashMap<String, Integer>());
    // Maps map1 = new Maps(new TreeMap<String, Integer>());
    Maps map1 = new Maps(new LinkedHashMap<String, Integer>());

    double time = System.currentTimeMillis() - timestart;
    System.out.println("All time: " + time/1000 + " sec");
  }
}
