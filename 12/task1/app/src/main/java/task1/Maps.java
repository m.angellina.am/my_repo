/*
Создать отображение размером в 2 000 000 записей. ok
Ключ - строка из 10 случайных символов.  ok
Значение - случайное число. ok
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.
 */
package task1;

import java.util.*;

public class Maps{
  Map mapp;

  Maps(Map<String,Integer> mapp){
    this.mapp = mapp;
    valuesGeneration();
    sortList();
  }

  public String keyGeneration(){
    Random a = new Random();
    String str = "";
    for (int i = 0; i < 10; i++) {
        str += (char) (a.nextInt(26) + 'a');
    }
    return str;
  }

  public void valuesGeneration(){
    double timestart = System.currentTimeMillis();

    for (int i = 0; i < 2000000; i++){
      int value = (int)(Math.random() * 10000);
      this.mapp.put(keyGeneration(), value);
    }

    double time = System.currentTimeMillis() - timestart;
    System.out.println("Time random generation: " + time/1000 + " sec");
  }

  public void sortList(){
    double startlink = System.currentTimeMillis();

    ArrayList<Integer> arrlist = new ArrayList<Integer>();
    Collection<Integer> coll = this.mapp.values();
    for (int i = 0; i < coll.size(); i++){
      arrlist.add(i);
    }
    double timelink = System.currentTimeMillis() - startlink;

    double timestart = System.currentTimeMillis();

    Collections.sort(arrlist);
    System.out.println(arrlist);

    double time = System.currentTimeMillis() - timestart;

    System.out.println("Time link: " + timelink/1000 + " sec");
    System.out.println("Time sort: " + time/1000 + " sec");
  }

  public void averageMap(){
    int count = 5;
    long time = 0;
    double averageMap = 0;

    for (int i = 1; i < count; i++){
      long timestart = System.currentTimeMillis();
      Maps m = new Maps(this.mapp);
      time = System.currentTimeMillis() - timestart;
    }

    averageMap = (time/count)/1000;
    System.out.println("------------------------------------------------------------");
    System.out.println("Среднее время у " + this.mapp.getClass() + " : " + averageMap + " sec");
  }
}
