### Task

```
Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.
```

### Diagram
[Diagram](https://app.diagrams.net/#G1eI3qOUkXl6vQ4gg55FRAjnzjrUEFAtLt)

#### hashmap

```
Time random generation: 4.243 sec
Time link: 0.045 sec
Time sort: 0.018 sec
All time: 4.324 sec


Time random generation: 4.344 sec
Time link: 0.055 sec
Time sort: 0.835 sec
All time: 5.234 sec
(with print)
```

#### treemap
```
Time random generation: 5.766 sec
Time link: 0.032 sec
Time sort: 0.015 sec
All time: 5.83 sec

Time random generation: 5.88 sec
Time link: 0.032 sec
Time sort: 0.452 sec
All time: 6.364 sec
(with print)
```

#### linkedhashmap
```
Time random generation: 3.694 sec
Time link: 0.039 sec
Time sort: 0.014 sec
All time: 3.76 sec

Time random generation: 3.627 sec
Time link: 0.038 sec
Time sort: 0.557 sec
All time: 4.222 sec
(with print)
```
