/*
Определить, является ли строка номером телефона.
Обязательно использование следующих конструкций:
группы, квантификаторы, классы символов, экранирование,
символы местоположения.
 */
package task1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
* class for checking a string for the presence of
* a phone number using regular expressions
* @author Angelina Motrikala
* @version 1.0
*/

public class Task1 {

  /**
  * point of entry
  * @param args string array
  */
  public static void main(String[] args) {
      System.out.println("Введите номер телефона");
      Scanner scanner = new Scanner(System.in);
      String phone = scanner.nextLine();
      System.out.println(checkPhone(phone));
  }
  /**
  * a method that checks if a string is a phone number
  * and is spelled correctly, taking into account all
  * the formalities
  * @param phone number
  * @return boolean
  */
  public static boolean checkPhone(String phone){
      String regexPhone = "^(\\+7|8)[\\(|\\-|\\s]?\\d{3}[\\)|\\-|\\s]?\\d{3}[\\-|\\s]?\\d{2}[\\-|\\s]?\\d{2}$";
      Pattern pattern = Pattern.compile(regexPhone);
      Matcher matcher = pattern.matcher(phone);
      return matcher.matches();
  }
}
