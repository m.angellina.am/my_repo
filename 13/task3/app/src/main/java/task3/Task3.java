/*
  Заменить упоминания нелюбимых вами слов на нужное любимое.
 */
 package task3;

 import java.util.regex.Matcher;
 import java.util.regex.Pattern;

 /**
 * class for replacing all unloved words
 * with favorite ones using regular expressions
 * @author Angelina Motrikala
 * @version 1.0
 */

 public class Task3 {

    /**
    * point of entry
    * @param args string array
    */
    public static void main(String[] args) {
      String str = "She liked to get things done in the morning";
      String[] unloved = new String[] {"morning", "afternoon"};
      String favorite = "nigth";
      System.out.println(checkText(str, unloved, favorite));
    }

    /**
    * method to replace all unloved words with favorite ones
    * @param text in which there may be unloved words
    * @param unloved words
    * @param favorite word
    * @return text in which now there are no unloved words
    */
    public static String checkText(String text, String[] unloved, String favorite){
      String regexText = unloved[0];
      for (int i = 1; i < unloved.length; i++){
        regexText += "|" + unloved[i];
      }
      Pattern pattern = Pattern.compile(regexText);
      Matcher matcher = pattern.matcher(text);
      text = matcher.replaceAll(favorite);
      return text;
    }
 }
