/*
 Предполагаем, что в тексте числительные идут всегда
 перед существительным, к которому они относятся.
 Вывести все существительные, о которых говорится, что они "пятые"
 */
package task2;

import java.util.regex.Matcher;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.Scanner;

/**
* class for displaying nouns that come
* after the numeral "fifth" using regular expressions
* @author Angelina Motrikala
* @version 1.0
*/

public class Task2 {

  /**
  * point of entry
  * @param args string array
  */
  public static void main(String[] args) {
      System.out.println("Введите строку: ");
      Scanner scanner = new Scanner(System.in);
      String text = scanner.nextLine();
      System.out.println(checkString(text));
  }

  /**
  * method for displaying nouns that come after the numeral "fifth"
  * @param text with the desired numeral
  * @return words that come after the numeral
  */
  public static String checkString(String text) {
      String regexStr = "[пП]ят(ый|ая|ое|ые|ого|ому|ом|ой|ую|ою|ых|ым|ыми)\\s+(\\w+)";
      Pattern pattern = Pattern.compile(regexStr, Pattern.UNICODE_CHARACTER_CLASS);
      Matcher matcher = pattern.matcher(text);
      String result = "";
      while (matcher.find()){
        MatchResult matchresult = matcher.toMatchResult();
        result += matchresult.group(2) + " ";
      }
      return result;
  }
}
