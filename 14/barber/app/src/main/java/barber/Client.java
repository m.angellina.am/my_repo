package barber;

/**
* класс Client, реализующий интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Client implements Runnable {

  /** имя клиента */
  String name;

  /** комната ожидания на рабочем месте парикмахера */
  private final Room room;

  /** рабочее место парикмахера */
  private final BarberChair chair;

  /**
  * конструктор Client
  * @param room - комната ожидания на рабочем месте парикмахера
  * @param chair - рабочее место парикмахера
  */
  Client(Room room, BarberChair chair) {
    this.room = room;
    this.chair = chair;
  }

  /**
   * метод, устанавливающий имя
   * @param name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * метод, отправляющий клиента на стрижку
   */
  private synchronized void goToChair() {
    room.leave();
    try {
      chair.occupy();
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }
  }

  /**
   * метод, отпускающий клиента домой
   */
  public synchronized void leaveChair() {
    chair.leave();
  }

  /**
   * метод, запускающий поток
   */
  public void run() {
    try {
      if (room.takePlace()) {
        System.out.println(this.name + " в комнате");
        chair.takePlaceInQueue();
        System.out.println(this.name + " сел на стул");
        goToChair();
        System.out.println(this.name + " подстригся и идет красивый домой");
        leaveChair();
      }
      else {
        System.out.println("Нет свободных мест! " + this.name + " уходит");
      }
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }
  }
}
