package barber;

/**
* enum класс BarberChairStatus
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public enum BarberChairStatus{
  OLD_OCCUPIED, NEW_OCCUPIED, FREE, QUEUED
}
