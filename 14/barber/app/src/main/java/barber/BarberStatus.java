package barber;

/**
* enum класс BarberStatus
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public enum BarberStatus{
  WORKING, SLEEPING, FREE
}
