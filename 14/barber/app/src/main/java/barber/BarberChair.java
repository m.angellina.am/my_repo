package barber;

import java.util.concurrent.Semaphore;

/**
* класс BarberChair
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class BarberChair{

  /** барбер, работающий на этом стуле */
  public Barber barber;

  /** место на стуле */
  Semaphore place = new Semaphore(1, true);

  /** статус стула */
  public BarberChairStatus status = BarberChairStatus.FREE;

  /**
   * метод для назначения барбера
   * @param barber - барбер, которого нужно назначить
   */
  public void setBarber(Barber barber) {
    this.barber = barber;
  }

  /**
   * метод для того, чтобы клиент занял стул
   * @throws InterruptedException
   */
  public synchronized void occupy() throws InterruptedException {
    this.status = BarberChairStatus.NEW_OCCUPIED;
    notifyAll();
    wait();
  }

  /**
   * метод, чтобы клиент освободил стул
   */
  public synchronized void leave() {
    if (this.place.hasQueuedThreads()){
      this.status = BarberChairStatus.QUEUED;
    } else {
      this.status = BarberChairStatus.FREE;
    }
    this.place.release();
    notifyAll();
  }

  /**
   * метод, чтобы парикмахер спал на стуле
   */
  public synchronized void sleepOnChair() {
    try {
      wait();
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }
  }

  /**
   * метод для занятия очереди на стул
   * @throws InterruptedException
   */
  public void takePlaceInQueue() throws InterruptedException {
    this.place.acquire();
  }

  /**
   * метод для оповещения клиента на стуле
   */
  public synchronized void ring() {
    notifyAll();
  }
}
