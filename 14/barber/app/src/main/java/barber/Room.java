package barber;

import java.util.concurrent.Semaphore;

/**
* класс Room
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Room{

  /** количество мест в комнате */
  public Semaphore countPlaces;

  /**
  * конструктор Room
  * @param countPlaces - количество мест в комнате
  */
  public Room(int countPlaces){
    this.countPlaces = new Semaphore(countPlaces, true);
  }

  /**
  * метод takePlace
  * метод, который позволяет занять место, если оно свободно
  * @return - boolean есть ли в комнате свободные места
  * @throws InterruptedException
  */
  public boolean takePlace() throws InterruptedException {
    return countPlaces.tryAcquire();
  }

  /**
  * метод isEmpty
  * метод, который проверяет пустая ли комната
  * @return - boolean пустая ли комната
  */
  public synchronized boolean isEmpty() {
    return (this.countPlaces.availablePermits() > 10);
  }

  /**
  * метод leave
  * метод для освобождения комнаты
  */
  public synchronized void leave() {
    this.countPlaces.release();
  }
}
