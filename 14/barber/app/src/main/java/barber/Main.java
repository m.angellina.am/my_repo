package barber;

public class Main {
  public static void main(String[] args) {
    Room room = new Room(10);
    BarberChair chair = new BarberChair();
    Barber barber = new Barber(room, chair);
    new Thread(new ClientsFlow(room, chair)).start();
    chair.setBarber(barber);
    new Thread(barber).start();
  }
}
