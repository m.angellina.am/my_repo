package barber;

/**
* класс Barber, реализующий интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Barber implements Runnable{

  /** комната, в которую приходят клиенты */
  Room room;

  /** рабочее место парикмахера, к которому приходят клиенты */
  BarberChair chair;

  /** статус барбера */
  BarberStatus status;

  /** начальное значение диапазона задержки */
  private static final int LATENCY_START = 3000;

  /** конечное значение диапазона задержки */
  private static final int LATENCY_FINISH = 7000;

  /** конструктор Barber
  * @param room - комната клиентов
  * @param chair - рабочее место парикмахера
  */
  public Barber(Room room, BarberChair chair) {
    this.room = room;
    this.chair = chair;
  }

  /**
   * метод, отправляющий парикмахера спать
   */
  public synchronized void goToSleep() throws InterruptedException {
    this.status = BarberStatus.SLEEPING;
    System.out.println("Барбер ушел спать...");
    this.chair.sleepOnChair();
  }

  /**
   * метод, отправляющий парикмахера работать
   */
  public synchronized void goToWork() {
    try {
      System.out.println("Барбер начинает работать");
      this.status = BarberStatus.WORKING;
      Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
      this.status = BarberStatus.FREE;
      System.out.println("Барбер закончил работать");
      chair.ring();
      chair.status = BarberChairStatus.OLD_OCCUPIED;
    } catch (InterruptedException e) {
        throw new RuntimeException(e);
    }
  }

  /**
   * метод для запуска потока
   */
  public void run() {
    System.out.println("Барбер приступает к работе");
    while (true) {
      try {
        if (chair.status == BarberChairStatus.NEW_OCCUPIED) {
          goToWork();
        }
        if (chair.status == BarberChairStatus.FREE) {
          if (room.isEmpty()) {
              goToSleep();
          }
        }
      } catch (InterruptedException e) {
          throw new RuntimeException(e);
      }
    }
  }
}
