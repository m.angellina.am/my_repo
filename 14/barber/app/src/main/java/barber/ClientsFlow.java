package barber;

/**
* класс ClientsFlow, реализующий интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class ClientsFlow implements Runnable {

  /** комната, в которую приходят клиенты */
  Room room;

  /** рабочее место парикмахера, к которому приходят клиенты*/
  BarberChair chair;

  /** начальное значение диапазона задержки */
  private static final int LATENCY_START = 3000;

  /** конечное значение диапазона задержки */
  private static final int LATENCY_FINISH = 7000;

  /**
  * конструктор ClientsFlow
  * @param room  - комната, в которую приходят клиенты
  * @param chair - рабочее место парикмахера
  */
  ClientsFlow(Room room, BarberChair chair) {
      this.room = room;
      this.chair = chair;
  }

  /**
  * метод для запуска потока
  */
  public void run() {
    int i = 0;
    while (true) {
      try {
        Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
        i++;
        Client client = new Client(room, chair);
        client.setName("Клиент " + i);
        new Thread(client).start();
      } catch (InterruptedException e) {
          throw new RuntimeException(e);
      }
    }
  }
}
