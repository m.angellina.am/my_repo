package readers_writers;

import java.util.*;

/**
* класс Reader наследуется от класса Thread
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Reader extends Thread {

  /** книга из которой мы читаем данные */
  private Book book;
  /** id читателя */
  private int id;

  /** начальное значение диапазона задержки */
  static final int START = 4000;
  /** конечное значение диапазона задержки */
  static final int FINISH = 7000;

  /** конструктор Reader
   * @param book - экземпляр класса Book
   * @param id - id читателя
   */
  Reader(Book book, int id) {
      this.book = book;
      this.id = id;
  }

  /** метод run
  * метод, который запускает поток
  */
  public void run() {
    String message = " ";
    while (message != ""){
      try {
        Thread.sleep(START + (int) (Math.random() * FINISH));
        message = this.book.read(id);
        System.out.println("Reader " + id + " прочитал: " + message);
      } catch (InterruptedException e) {
          e.printStackTrace();
      }
    }
  }
}
