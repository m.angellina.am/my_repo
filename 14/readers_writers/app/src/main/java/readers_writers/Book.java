package readers_writers;

import java.util.concurrent.Semaphore;

/**
* класс Book
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Book {

  /** поле, которое хранит сгенерированную строку */
  private String content;

  /** семафор разрешения на взаимодействие с книгой */
  public Semaphore permit = new Semaphore(1, true);

  /** начальное значение диапазона задержки */
  static final int START = 4000;
  /** конечное значение диапазона задержки */
  static final int FINISH = 7000;

  /** геттер
  * @return - content
  */
  public String getContent(){
    return this.content;
  }

  /**
   * метод write записывает в поле content строку
   * @param content - строка
   * @throws InterruptedException
   */
  public synchronized void write(String content, int id) throws InterruptedException {
    this.permit.acquire();
    System.out.println("Writer " + id + " получил разрешение");
    System.out.println("Writer " + id + " начинает писать");
    Thread.sleep(START + (int) (Math.random() * FINISH));
    System.out.println("Writer " + id + " записал: " + content);
    this.content = content;
    this.permit.release();
  }

  /**
   * метод read берет из поля данных строку
   * @return content - строка
   * @throws InterruptedException
   */
  public synchronized String read(int id) throws InterruptedException {
    if (this.permit.availablePermits() == 0 && this.content == null) {
        wait();
    }
    else if (this.permit.availablePermits() == 1){
      System.out.println("Reader " + id + " получил разрешение");
      System.out.println("Reader " + id + " начинает читать");
      Thread.sleep(START + (int) (Math.random() * FINISH));
    }
    return this.content;
  }
}
