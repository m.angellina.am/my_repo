package readers_writers;

import java.util.*;

/**
* класс Reader наследуется от класса Thread
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Writer extends Thread {

  /** книга из которой мы читаем данные */
  private Book book;
  /** id читателя */
  private int id;

  /** поле, которое хранит длину генерируемой строки*/
  static final int SIZE_GENERATION_STRING = 15;
  /** начальное значение диапазона задержки */
  static final int START = 4000;
  /** конечное значение диапазона задержки */
  static final int FINISH = 7000;

  /** конструктор Writer
   * @param book - экземпляр класса Book
   * @param id - id читателя
   */
  Writer(Book book, int id) {
      this.book = book;
      this.id = id;
  }

  /**
   * метод generationString
   * генерирует случайное строку
   * @return name - сгенерированная строка
   */
  private static String generationString() {
    String name = "";
    Random random = new Random();
    for(int i = 0; i < SIZE_GENERATION_STRING; i++) {
        char c = (char)(random.nextInt(26) + 'a');
        name += c;
    }
    return name;
  }

  /** метод run
  * метод, который запускает поток
  */
  public void run() {
    while (true) {
      try {
        this.book.write(generationString(), id);
      } catch (InterruptedException e) {
          throw new RuntimeException(e);
      }
    }
  }
}
