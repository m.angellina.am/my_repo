package readers_writers;

public class Main {
  public static void main(String[] args) {
    Book book = new Book();
    Thread[] threads = {
      new Thread(new Writer(book, 1)),
      new Thread(new Writer(book, 2)),
      new Thread(new Reader(book, 3)),
      new Thread(new Reader(book, 4)),
      new Thread(new Reader(book, 5))
    };

    for (int i = 0; i < threads.length; i++) {
      threads[i].start();
    }
  }
}
