package smokers;

/**
* класс Smoker реализует интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/

public class Smoker implements Runnable{
   /** начальное значение диапазона задержки */
  private static final int LATENCY_START = 3000;

   /** конечное значение диапазона задержки */
  private static final int LATENCY_FINISH = 5000;

  /** имя курильщика */
  private String name;

  /** экземпляр класса Resource */
  private Resource resource;

  /** стол, за которым сидят курильщики */
  private Table table;

  /** статус курильщика */
  private SmokerStatus status = SmokerStatus.WAITING;

  /**
   * конструктор Smoker
   * @param name - имя курильщика
   * @param Resource - ресурсы
   * @param table - стол, за которым сидят курильщики
   */
  Smoker(String name, Resource resource, Table table) {
    this.name = name;
    this.resource = resource;
    this.table = table;
  }

  /**
   * метод getStatus
   * @return status - статус курильщика
   */
  public SmokerStatus getStatus() {
    return status;
  }

  /**
   * метод getResource
   * @return класс Resource
   */
  public synchronized Resource getResource() {
    return resource;
  }

  /**
   * метод, который проверяет наличие нужных ресурсов на столе
   * @return boolean
   */
  public boolean allResourcesOnTheTable() {
    switch (this.resource) {
      case PAPER:
        return table.hasMatches() && table.hasTobacco();
      case MATCHES:
        return table.hasPaper() && table.hasTobacco();
    }
  return table.hasMatches() && table.hasPaper();
  }

  /**
   * метод, который берет необходимые ресурсы со стола
   * @return Resources
   */
  public Resource[] takeResourcesFromTable() {
    switch (this.resource) {
      case PAPER:
        return new Resource[]{table.takeMatches(), table.takeTobacco()};
      case MATCHES:
        return new Resource[]{table.takePaper(), table.takeTobacco()};
      case TOBACCO:
        return new Resource[]{table.takeMatches(), table.takePaper()};
    }
    return null;
  }

  /**
   * метод, запускающий поток
   */
  public void run() {
    while (true) {
      try {
        status = SmokerStatus.WAITING;
        Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
        if (allResourcesOnTheTable()) {
          takeResourcesFromTable();
          status = SmokerStatus.SMOKING;
          System.out.println(this.name + " курит");
          Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
          System.out.println(this.name + " закончил курить");
        }
        else {
          System.out.println(this.name + " ресурсов для курения не обнаружил");
        }
      } catch (InterruptedException e) {
          throw new RuntimeException(e);
      }
    }
  }
}
