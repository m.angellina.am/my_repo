package smokers;

/**
* enum класс SmokerStatus
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public enum SmokerStatus {
  WAITING, SMOKING
}
