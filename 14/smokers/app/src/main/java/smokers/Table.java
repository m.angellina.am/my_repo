package smokers;

import java.util.ArrayList;

/**
* класс Table
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Table {

  /** наличие спичек */
  private boolean hasMatches;

  /** наличие табака */
  private boolean hasTobacco;

  /** наличие бумаги */
  private boolean hasPaper;

  /**
   * метод hasMatches
   * проверяет есть ли спички
   * @return boolean
   */
  public boolean hasMatches() {
    return hasMatches;
  }
  /**
   * метод hasTobacco
   * проверяет есть ли табак
   * @return boolean
   */
  public boolean hasTobacco() {
    return hasTobacco;
  }

  /**
   * метод hasPaper
   * проверяет есть ли бумага
   * @return boolean
   */
  public boolean hasPaper() {
    return hasPaper;
  }

  /**
    * метод takeMatches
    * выдает спички, если они есть
    * @return Resourse
    */
   public synchronized Resource takeMatches() {
     if (hasMatches()) {
       hasMatches = false;
       System.out.println("Взяли спички");
       return Resource.MATCHES;
     }
     else {
       System.out.println("Спичек нет");
       return null;
     }
   }

   /**
    * метод takeTobacco
    * выдает табак, если он есть
    * @return Resourse
    */
   public synchronized Resource takeTobacco() {
     if (hasTobacco()) {
       hasTobacco = false;
       System.out.println("Взяли табак");
       return Resource.TOBACCO;
     }
     else {
       System.out.println("Табака нет");
       return null;
     }
   }

   /**
    * метод takePaper
    * выдает бумагу, если она есть
    * @return Resourse
    */
   public synchronized Resource takePaper() {
     if (hasPaper()) {
       hasPaper = false;
       System.out.println("Взяли бумагу");
       return Resource.PAPER;
     } else {
       System.out.println("Бумаги нет");
       return null;
     }
   }

   /**
     * метод, который кладет ресурсы на стол
     * @param resources - лист ресурсов
     */
    public void putResources(ArrayList<Resource> resources) {
      for (Resource resource : resources) {
        switch (resource) {
          case MATCHES:
            if (!hasMatches) {
                System.out.println("На стол положили спички");
                hasMatches = true;
            }
            break;
          case TOBACCO:
            if (!hasTobacco) {
                System.out.println("На стол положили табак");
                hasTobacco = true;
            }
            break;
          case PAPER:
            if (!hasPaper) {
                System.out.println("На стол положили бумагу");
                hasPaper = true;
            }
            break;
        }
      }
    }
}
