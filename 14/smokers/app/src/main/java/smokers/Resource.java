package smokers;

/**
* enum класс Resource
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public enum Resource {
  MATCHES, TOBACCO, PAPER
}
