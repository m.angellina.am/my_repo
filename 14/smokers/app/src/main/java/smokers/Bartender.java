package smokers;

import java.util.ArrayList;

/**
* класс Bartender реализует интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Bartender implements Runnable {
  /** начальное значение диапазона задержки */
  private static final int LATENCY_START = 4000;

  /** конечное значение диапазона задержки */
  private static final int LATENCY_FINISH = 7000;

  /** стол, который обслуживает бармен */
  private Table table;

  /** курильщики */
  private Smoker[] smokers;

  /** список курильщиков */
  private ArrayList<Smoker> waitingSmokers = new ArrayList<>();

  /**
   * конструктор Bartender
   * @param table - стол, который обслуживает бармен
   * @param smokers - список курильщиков
   */
  Bartender(Table table, Smoker[] smokers) {
    this.table = table;
    this.smokers = smokers;
  }

  /**
   * метод updateSmokers
   * обновляет всех ожидающих курильщиков
   */
  private void updateSmokers() {
    waitingSmokers.clear();
    for (Smoker smoker : smokers) {
      if (smoker.getStatus() == SmokerStatus.WAITING) {
          waitingSmokers.add(smoker);
      }
    }
  }

  /**
   * метод, который запускает поток
   */
  public void run() {
    while (true) {
      try {
        ArrayList<Resource> takeResources = new ArrayList<>();
        Thread.sleep(LATENCY_START + (int) (Math.random() * LATENCY_FINISH));
        updateSmokers();
        if (waitingSmokers.size() >= 2) {
          System.out.println("Бармен кладет ресурсы на стол");
          for (int i = 0; i < 2; i++) {
              takeResources.add(waitingSmokers.get((int)(Math.random() * waitingSmokers.size())).getResource());
          }
          table.putResources(takeResources);
          takeResources.clear();
        }
        else {
          System.out.println("Бармен не может взять ресурсы");
        }
      } catch (InterruptedException e) {
          throw new RuntimeException(e);
      }
    }
  }
}
