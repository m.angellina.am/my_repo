package philosophers;

public class Main{
  public static void main(String[] args) {
    final int COUNT = 5;

    Waiter waiter = new Waiter(COUNT);
    Fork[] forks = new Fork[COUNT];
    Philosopher[] philosophers = new Philosopher[COUNT];

    for (int i = 0; i < COUNT; i++){
        forks[i] = new Fork();
    }

    for (int i = 0; i < COUNT - 1; i++){
        philosophers[i] = new Philosopher("Философ " + (i + 1), forks[i], forks[i + 1], waiter);
    }

    philosophers[COUNT - 1] = new Philosopher("Философ " + COUNT, forks[COUNT - 1], forks[0], waiter);

    for (Philosopher philosopher: philosophers){
        new Thread(philosopher).start();
    }
  }
}
