package philosophers;

import java.util.concurrent.Semaphore;

/**
* класс Waiter
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Waiter {
  /**
  * необходимое количество вилок для принятия пищи
  */
  private static final int COUNT_FORK = 2;

  /**
  * семафор для использования вилок
  */
  public Semaphore forks;

  /**
  * конструктор официанта
  * @param countFork - количество доступных вилок
  */
  Waiter(int countFork) {
      this.forks = new Semaphore(countFork, true);
  }

  /**
  * метод accessToFork
  * запрашивает разрешение на использование вилок
  */
  public void accessToFork() {
      try {
          this.forks.acquire(COUNT_FORK);
      } catch (InterruptedException e) {}
  }

  /**
  * метод giveFork
  * кладет вилки обратно на стол
  */
  public void giveFork() {
      this.forks.release(COUNT_FORK);
  }
}
