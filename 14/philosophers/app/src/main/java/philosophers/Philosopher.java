package philosophers;

/**
* класс Philosopher, использующий интерфейс Runnable
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Philosopher implements Runnable{
  /** статус философа */
  public PhilosopherStatus status = PhilosopherStatus.THINK;
  /** имя философа */
  public String name;
  /** левая вилка */
  public Fork leftFork;
  /** правая вилка */
  public Fork rightFork;
  /** официант, обслуживающий стол */
  public Waiter waiter;

  /** начальное значение диапазона задержки */
  private static final int LATENCY_START = 5000;
  /** конечное значение диапазона задержки */
  private static final int LATENCY_END = 3000;

  /**
  * конструктор Philosopher
  * @param name - имя философа
  * @param leftFork - левая вилка
  * @param rightFork - правая вилка
  * @param waiter - официант, обслуживающий стол
  */
  Philosopher(String name, Fork leftFork, Fork rightFork, Waiter waiter) {
    this.name = name;
    this.leftFork = leftFork;
    this.rightFork = rightFork;
    this.waiter = waiter;
  }

  /** метод run
  * метод, который запускает поток
  */
  public void run() {
    while (true) {
        try {
          this.waiter.accessToFork();
          if (this.leftFork.isAvailable() && this.rightFork.isAvailable()) {
            this.leftFork.takeFork();
            this.rightFork.takeFork();
            System.out.println(this.name + " взял левую и правую вилки");

            System.out.println(this.name + " начал есть спагетти");
            this.status = PhilosopherStatus.EAT;
            Thread.sleep(LATENCY_START + (int)(Math.random() * LATENCY_END));
            System.out.println(this.name + " закончил есть");

            this.leftFork.putFork();
            this.rightFork.putFork();
            System.out.println(this.name + " положил левую и правую вилки");

            System.out.println(this.name + " начал философствовать");
            this.status = PhilosopherStatus.THINK;
          }
          this.waiter.giveFork();
        } catch (InterruptedException e) {}
    }
  }
}
