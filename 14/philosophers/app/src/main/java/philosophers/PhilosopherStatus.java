package philosophers;

/**
* enum класс PhilosopherStatus
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public enum PhilosopherStatus{
  THINK, EAT
}
