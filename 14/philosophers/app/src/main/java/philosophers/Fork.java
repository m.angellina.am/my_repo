package philosophers;

import java.util.concurrent.Semaphore;

/**
* класс Fork
* @author Motrikala Angelina
* version 1.0
* @see Main
*/
public class Fork {

  /** семафор для отслеживания того, заняты ли вилки */
  Semaphore sem = new Semaphore(1, true);

  /**
  * метод takeFork
  * метод, чтобы взять вилку
  */
  public void takeFork() {
    try {
        this.sem.acquire();
    } catch (InterruptedException e) {}
  }

  /**
  * метод putFork
  * метод, чтобы положить вилку
  */
  public void putFork() {
    this.sem.release();
  }

  /**
  * метод isAvailable
  * метод для проверки, свободна ли вилка
  * @return boolean
  */
  public boolean isAvailable(){
    return this.sem.availablePermits() > 0;
  }
}
