package DoubleConnectionList;
import java.util.List;
import java.util.Collection;
import java.util.ListIterator;
import java.util.Iterator;


class DoubleConnectionList<E> implements List{
  public Node head = null;
  public Node tail = null;


  class Node{
    private Node next = null;
    private Node previous = null;
    private E data = null;

    Node(){
      this.next = null;
      this.previous = null;
      this.data = null;
    }

    Node(E data){
      this.next = null;
      this.previous = null;
      this.data = data;
    }

    public E getData(){
      return this.data;
    }

    public Node getNext(){
      return this.next;
    }

    public void setData(E newData){
      this.data = newData;
    }

    public void setNext(Node newNext){
      this.next = newNext;
    }

    public void setPrevious(Node newPrevious){
      this.previous = newPrevious;
    }

    public Node getPrevious(){
      return this.previous;
    }
  }


  public Object[] toArray(Object[] a){
    int size = this.size();
    a = new Object[size];
    Node node = this.head;
    for (int i = 0; i < size; i++) {
      a[i] = node.getData();
      node = node.getNext();
    }
    return a;
  }

  public E[] toArray(){
    int size = this.size();
    E[] arr = (E[]) new Object[size];
    Node node = this.head;
    for (int i = 0; i < size; i++) {
      arr[i] = node.getData();
      node = node.getNext();
    }
    return arr;
  }

  interface DoubleConnectionIterator extends Iterator { }

  private class NodeIterator implements DoubleConnectionIterator{

    Node currentNode = head;

    public boolean hasNext(){
      return currentNode != null;
    }

    public E next(){
      E result = currentNode.getData();
      currentNode = currentNode.getNext();
      return result;
    }
  }


  public DoubleConnectionIterator iterator(){
    DoubleConnectionIterator iterator = this.new NodeIterator();
    return iterator;
  }

  public void printAll(){
    DoubleConnectionIterator iterator = this.new NodeIterator();
    while (iterator.hasNext()) {
      System.out.print(iterator.next() + " ");
    }
      System.out.println();
  }


  public boolean add(Object data){
    Node node = new Node((E)data);
    if(this.head == null){
      this.head = node;
      this.tail = node;
    } else {
      Node oldTail = this.tail;
      oldTail.setNext(node);
      this.tail = node;
      this.tail.setPrevious(oldTail);
    }
    return true;
  }


  public boolean addAll(Collection collection){
    E[] arr = (E[]) collection.toArray();
    for (int i = 0; i < arr.length; i++) {
      if (this.add(arr[i]) == false)
        return false;
    }
    return true;
  }


  public int size(){
    Node node = this.head;
    int size = 0;
    while (node != null) {
      size++;
      node = node.getNext();
    }
    return size;
  }


  public void clear(){
    while (this.head != null) {
      Node next = this.head.getNext();
      this.head.setData(null);
      this.head.setNext(null);
      this.head.setPrevious(null);
      this.head = next;
    }
    this.tail = null;
  }


  public boolean contains(Object o){
    Node node = this.head;
    while (node != null){
      if(node.getData() == o){
        return true;
      }
      node = node.getNext();
    }
    return false;
  }


  public boolean containsAll(Collection collection){
    if (this.size() < collection.size()) {
      return false;
    }
    Iterator it2 = collection.iterator();
    while (it2.hasNext()){
      if (!(this.contains(it2.next()))) {
        return false;
      }
    }
    return true;
  }


  public boolean isEmpty(){
    return this.size() == 0;
  }
  

  public boolean remove(Object o){
    Node node = this.head;
    try{
        while (node != null){
          if (node.getData() == (E) o){
            if (node.getNext() != null){
              Node next = node.getNext();
              if (node != this.head){
                node.getPrevious().setNext(next);
                next.setPrevious(node.getPrevious());
                node.setData(null);
                node.setPrevious(null);
                node.setNext(null);
                return true;
              } else {
                this.head = node.getNext();
                node.getNext().setPrevious(null);
                node.setData(null);
                node.setPrevious(null);
                node.setNext(null);
                return true;
              }
            } else {
              if (node == this.head){
                node.setData(null);
                node.setPrevious(null);
                node.setNext(null);
                return true;
              } else {
                node.getPrevious().setNext(null);
                node.setData(null);
                node.setPrevious(null);
                node.setNext(null);
                return true;
              }
            }
          }
          node = node.getNext();
        }
    } catch (NullPointerException|UnsupportedOperationException e){
      System.out.println(e);
      return false;
    }
    return false;
  }

  public boolean removeAll(Collection collection){
      Object[] arr = collection.toArray();
      for (int i = 0; i < arr.length; i++) {
        if (this.remove(arr[i]) == false){
            return false;
        }
      }
      return true;
  }

  public boolean equals(Object o){
    if (!(o instanceof DoubleConnectionList)){
      return false;
    }
    DoubleConnectionList newList = (DoubleConnectionList) o;
    if(this.size() != newList.size()){
      return false;
    }
    Node node = this.head;
    Node newNode = newList.head;
    while (node != null) {
      if (node.getData() != newNode.getData()) {
        return false;
      }
      node = node.getNext();
      newNode = newNode.getNext();
    }
    return true;
  }

  public E get(int index){
    try{
      Node node = this.head;
      for (int i = 0; i < index; i++) {
        node = node.getNext();
      }
      return node.getData();
    } catch (ArrayIndexOutOfBoundsException e) {
      return null;
    }
  }

  public boolean retainAll(Collection c){
    throw new UnsupportedOperationException();
  }

  public List<E> subList(int fromIndex, int toIndex){
    throw new UnsupportedOperationException();
  }

  public ListIterator listIterator(){
    throw new UnsupportedOperationException();
  }

  public ListIterator listIterator(int c){
    throw new UnsupportedOperationException();
  }

  public int lastIndexOf(Object o){
    throw new UnsupportedOperationException();
  }

  public int indexOf(Object o){
    int counter = 0;
    DoubleConnectionIterator iterator = this.iterator();
    while (iterator.hasNext()){
      if (iterator.next() == o){
        return counter;
      }
      counter++;
    }
    return -1;
  }

  public E remove(int index){
    int counter = 0;
    Node node = this.head;
    E result;
    try{
      while (node != null){
        if (counter == index){
          result = node.getData();
          if (node.getNext() != null){
            Node next = node.getNext();
            if (node != this.head){
              node.getPrevious().setNext(next);
              next.setPrevious(node.getPrevious());
              node.setData(null);
              node.setPrevious(null);
              node.setNext(null);
              return result;
            } else {
              this.head = node.getNext();
              node.setData(null);
              node.setPrevious(null);
              node.setNext(null);
              return result;
            }
          } else {
            if (node == this.head){
              node.setData(null);
              node.setPrevious(null);
              node.setNext(null);
              return result;
            } else {
              node.getPrevious().setNext(null);
              node.setData(null);
              node.setPrevious(null);
              node.setNext(null);
              return result;
            }
          }
        }
        node = node.getNext();
        counter++;
      }
    } catch (NullPointerException|UnsupportedOperationException e){
      System.out.println(e);
      return null;
    }
    return null;
  }

  public void add(int index, Object element){
    int counter = 0;
    Node node = this.head;
    Node newNode = new Node((E) element);
    while (node != null){
      if (counter == index){
        if (node == this.head) {
          this.head = newNode;
          newNode.setNext(node);
          return;
        } else {
          node.getPrevious().setNext(newNode);
          newNode.setPrevious(node.getPrevious());
          newNode.setNext(node);
          return;
        }
      }
      node = node.getNext();
      counter++;
    }
  }

  public Object set(int index, Object element){
    int counter = 0;
    Node node = this.head;
    while (node != null){
      if (counter == index){
        E data = node.getData();
        node.setData((E) element);
        return data;
      } else {
        node = node.getNext();
        counter++;
      }
    }
    return null;
  }

  public boolean addAll(int index, Collection c){
    throw new UnsupportedOperationException();
  }
}
